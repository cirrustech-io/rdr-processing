#!/usr/bin/python
'''
@author: Ali Khalil
@summary: Archive RDR files by adding information to database and then moving to archive target folder along with file rename.
'''

import syslog
import sys
import datetime
import os
from corp.bras.rdr.repository import Repository
from corp.bras.rdr.rdr import RDR
from corp.bras.rdr.database import Database

try:
    
    source = '/data/cm/'
    target_mount = '/data/target/'
    target = '/data/target/archive/'
    
    # Load MySQL database parameters from config_db.py
    from config_db import *
    
    # Validate that mounts for source and target locations of RDRs exist. 
    # Abort if it doesn't. 
    if os.path.ismount(source) and os.path.ismount(target_mount):
        repository = Repository()
        
        # Set locations
        repository.location_source = source
        repository.location_archive = target
    
        directories = repository.getDirectoriesList() 
        files=[]
        
        # Initialize the database connection
        db = Database()
        db.mysql_host = mysql_host
        db.mysql_user = mysql_user
        db.mysql_db = mysql_db
        db.mysql_passwd = mysql_passwd
        db.prime()
        
        for directory in directories:
            # Get list of files
            files = repository.getFilesList_walk( directory )
            syslog.syslog( '''Processing %s files in directory %s%s'''%(len(files), repository.location_source, directory) )
            
            if len(files) > 0:
                for file in files:
                    file_path = os.path.normcase(repository.location_source + directory + '/' + file)
                    
                    rdr = RDR(file_path)
                    rdr.path_target = repository.location_archive
                
                    # Only perform actions if the RDR file can be processed    
                    if rdr.process():
                        if db.existsRDR(rdr):
                            # Delete the file if RDR exists in database
                            rdr.delete()
                        else:
                            if db.insertRDR(rdr):
                            #if db.loadRDR(rdr):
                                rdr.archive()
                    else:
                        # Delete blank or invalid files
                        rdr.delete()
                    del rdr
            else:
                syslog.syslog( "No files found in directory. %s"%(directory) )

    else:
        syslog.syslog( '''One or both mounts do not exist. Aborting process''' )
    
except:
    syslog.syslog( '''Error - Could not process Archive''' )
    syslog.syslog( str(sys.exc_info()) )

