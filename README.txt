DESCRIPTION:
------------
This Python package is designed to process RDR CSV files 
in to a MySQL database and move the original file in to 
an archival directory with a rename.


SOURCE DIRECTORY:
-----------------
Source directory is where the CSV Adapter stores all csv files. On a production
Collection Manager server the path should be: 
	/opt/cm/cm/adapters/CSVAdapter/csvfiles
	
The source directory must have directories named after RDR Tags i.e.
 
`-- csvfiles
    |-- 4042321920
    |-- 4042321922
    |-- 4042321924
    |-- 4042321925
    |-- 4042321970
    |-- 4042321984
    |-- 4042322000
    |-- 4042322032
    |-- 4042322033
    |-- 4042322034
    |-- 4042322035
    |-- 4042322048
    |-- 4042323004
    |-- 4042323050
    `-- 4042323260


MYSQL SCHEMA:
-------------
The MySQL database schema is located in the file mysql/cscms-rdrs.sql. The username and 
password for accessing and modifying the database must be created manually and the information
must then be reflected in the class.


DATABASE CONFIGURATION:
-----------------------
The config_db.py file must be modified to reflect the database details
