#!/usr/bin/env python

from distutils.core import setup

setup(name='cscms-processing-archival',
      version='1.0',
      description='Cisco Service Control Engine generated RDRs containing subscriber usage data which are stored as CSV files by the CSV Adapter. These CSV files using this script can be processed in to a MySQL Database and then archived in to a specified directory.',
      author='Ali M. Khalil',
      author_email='ali.khalil@2connectbahrain.com',
      url='http://www.2connectbahrain.com',
      packages=['corp.bras.rdr'],
     )
