'''
Created on Jun 6, 2012

@author: Ali Khalil
'''

import sys
import syslog
import datetime
import os
import re
import commands
import csv
import shutil

#TODO: Verify rouce and target locations exist
                                    
class Repository(object):


    def __init__(self):
        '''
        Initialize the NpcdbDump Object
        '''
        try:
            # Directory locations
            self.location_source = ''
            self.location_archive = ''
            
            # Loaded records count
            self.count = -1
            
            # Process ID for inclusion in logs
            self.pid = os.getpgid(0)
            
            # Get current datetime
            self.runtime = datetime.datetime.now()
            
            # Currently run script file name
            self.fname = sys.argv[0]
            
            self.ignoreTmp = re.compile('.*\.tmp$')
            
            syslog.openlog('''rdr-process[%s]'''%(self.pid))
            
            self.log('Initialized RDR Repository instance with pid %s.'%(self.pid))
            
            self.timer_begin = datetime.datetime.now()
             
        except:
            self.log('''INIT: Error - Couldn't initialize repository instance.''')
            self.log( str( sys.exc_info() ) )
            return False
    
    
    
    def __del__(self):
        '''
        Cleanup
        '''
        self.log('''Ending Repository instance''')
        self.log( '''Processing duration: %s'''%( datetime.datetime.now() - self.timer_begin ) )
        
        
    def getDirectoriesList(self):
        '''
        Get a list of all directories in source path
        '''
        try:
            directories = []
            for item in os.listdir( self.location_source ):
                item_fullpath = self.location_source + item
                if os.path.isdir( item_fullpath ):
                    directories.append(item)
            return directories
        except:
            self.log( '''Error - cannot get list of directories.''' )
            self.log( str( sys.exc_info() ) )
            return False

    def getFilesList(self, directory):
        '''
        Get list of files from directory
        '''
        try:
            files = []
            location = self.location_source + directory + '/'
    
            for item in os.listdir( location ):
                item_fullpath = location + item
                if os.path.isfile( item_fullpath ):
                    files.append(item)
            return files
        except:
            self.log( '''Error - cannot get list of files.''' )
            self.log( str( sys.exc_info() ) )
            return False


    def getFilesList_walk(self, directory):
        '''
        Get list of files recursively from directory
        '''
        try:
            files = []
            location = self.location_source + directory + '/'
            basepath = os.path.normpath(self.location_source + '/' + directory) + '/'
    
            for item in os.walk( location ):
                # If item is list of files item[1] is an empty list 
                if not item[1]:
                    for file in item[2]:
                        file_fullpath = os.path.normpath(item[0] + '/' + file)
                        file_relpath = re.sub(basepath, '', file_fullpath)
                        # Check if item is of type 'file' and it is not a temporary file
                        if os.path.isfile( file_fullpath ) and not self.ignoreTmp.match(file):
                            files.append(file_relpath)
            return files
        except:
            self.log( '''Error - cannot get list of files.''' )
            self.log( str( sys.exc_info() ) )
            return False



    def log(self, message):
        '''
        Output error to either stdout or syslog
        '''
        # Method options stdout or syslog
        methods = [ 'stdout', 'syslog' ]
        method = methods[1]
        try:
            if method == 'syslog':
                syslog.syslog( message )
            else:
                print message
            return True
        except:
            print '''Error - cannot log messages.'''
            print str( sys.exc_info() )
            return False
    

