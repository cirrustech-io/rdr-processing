'''
Created on Jun 12, 2012

@author: Ali Khalil
'''

import sys
import syslog
import datetime
import os
import re
import MySQLdb
            
                                    
class Database(object):

    def __init__(self):
        '''
        Initialize the NpcdbDump Object
        '''
        try:
            # MySQL related configuration parameters
            # MySQL server details to be added in to config_db.py file
            self.mysql_host = ''
            self.mysql_user = ''
            self.mysql_passwd = ''
            self.mysql_db = ''

            
            # RDR Tag to Name translation table
            self.translationTable = 'tags_to_names'
            self.translations = {}
            
            self.tableDefinitions = {}

                # Number of columns to ignore in output of DESCRIBE
            self.columnsIgnore = 3
            
            # Loaded records count
            self.count = -1
            
            self.log('Initialized RDR Database instance.')
             
        except:
            self.log('''INIT: Error - Couldn't initialize database instance.''')
    
    
    def prime(self):
        '''
        Process priming functions to load basic data in to object
        '''
        try:
            self.db = self.rdrDB()
            self.getTagTranslations()
            self.genTableDefinitions()
            return True
        except:
            self.log( '''Cannot prime the database object.''' )
            self.log( str( sys.exc_info() ) )
            return False
        
    
    def __del__(self):
        '''
        Cleanup
        '''
        # Close the database connection before exiting program
        try:
            self.db.close()
            self.log('''Ending Database instance''')
            return True
        except:
            self.log( '''Problem closing connection to database.''' )
            self.log( str( sys.exc_info() ) )
            return False
        
        
    def rdrDB(self):
        '''
        Database object
        '''
        try:
            db = MySQLdb.connect( host=self.mysql_host, user=self.mysql_user, passwd=self.mysql_passwd, db=self.mysql_db)
            return db
        except MySQLdb.Error, e:
            self.log( '''Problem connecting to database''' )
            self.log( "Error %d: %s" % (e.args[0], e.args[1]) )
            self.log( str( sys.exc_info() ) )
            return False
    
    def getTagTranslations(self):
        '''
        Get the list of Tag to Name translations
        '''
        try:
            cur = self.db.cursor()
            sql_getTranslations = "SELECT tag, name FROM `%s`.`%s`;"%(self.mysql_db, self.translationTable)
            cur.execute( sql_getTranslations )
            data = cur.fetchall()
            
            translations = {}
            for tag in data:
                translations[ tag[0] ] = tag[1]
            self.translations = translations
            # TODO: Error if no translations found!
        
        except MySQLdb.Error, e:
            self.log( "Error %d: %s" % (e.args[0], e.args[1]) )
            self.log( str( sys.exc_info() ) )
            return False
     
    
            
    def getTableColumns(self, table):
        '''
        Get the list of Tag to Name translations
        '''
        try:
            cur = self.db.cursor()
            cur.execute("DESCRIBE `%s`.`%s`"%(self.mysql_db, table))
            description = cur.fetchall()
            
            columns = []
            significant_columns = len(description) - self.columnsIgnore
            
            for idx, column in enumerate(description):
                # Ignore last columns
                if idx < significant_columns:
                    columns.append( column[0] )
                else:
                    break
                
            return tuple(columns)
            
        except MySQLdb.Error, e:
            self.log( "Error %d: %s" % (e.args[0], e.args[1]) )
            self.log( str( sys.exc_info() ) )
            return False
    
    
    def genTableDefinitions(self):
        '''
        Get and store table definitions
        '''
        try:
            definitions = {}
            for tag, table in self.translations.items():
                definitions[tag] = self.getTableColumns(table)
            self.tableDefinitions = definitions
        except:
            self.log( '''Error - Could not generate table definitions''' )
            self.log( str( sys.exc_info() ) )
            return False
    
    def existsRDR(self, rdr):        
        '''
        Check to see if RDR has been already processed
        '''
        
        try:
            # Start database cursor
            cursor = self.db.cursor()
            
            # FILES IMPORTED
            filesImported_check = '''SELECT COUNT(*) FROM `filesImported` WHERE (md5sum='%s' OR sha1_hash='%s') AND  processed=1;'''%(rdr.md5sum, rdr.sha1_hash)
            cursor.execute( filesImported_check )
            exists = cursor.fetchone()[0]

            self.db.commit()
            cursor.close()
            if exists > 0:
                return True
            else:
                return False

        except MySQLdb.Error, e:
        #except:
            self.log( "Error %d: %s" % (e.args[0], e.args[1]) )
            self.log( '''Error - Could not check if RDR exists.''')
            self.log( str( sys.exc_info() ) )
            return False
        
        
        
        
    def insertRDR(self, rdr):
        '''
        Add records from RDR file in to database using INSERT statement
        '''
        
        #if self.existsRDR(rdr):
            #self.log('''Skip processing file: %s'''%(rdr.file))
            #return False
        

        try:
            # Start database cursor
            cursor = self.db.cursor()
            
            table = self.translations[rdr.tag]
            
            # FILES IMPORTED
            filesImported_insert = '''INSERT INTO `filesImported`( rdr, filename, rows, md5sum, sha1_hash, processed) VALUES('%s', '%s', '%s', '%s', '%s', '%s')'''%(rdr.tag, rdr.path_target, rdr.num_records, rdr.md5sum, rdr.sha1_hash, 0)
            cursor.execute( filesImported_insert )
            filesImported_id = cursor.lastrowid
            
            # Remove apostrophe (') from table column names
            columns = re.sub( "'", "", str(self.tableDefinitions[rdr.tag]) )

            # Add column name for csvFile
            columns = re.sub( '\)$', ', csvFile)', columns)
            
            # Generate beginning of INSERT statement
            insert_statement = '''INSERT INTO `%s` %s VALUES\n'''%(table, columns )
            
            # Add csvFile column to INSERT statement 
            csvFile = ''', '%s')'''%(filesImported_id)
            
            for idx, record in enumerate(rdr.records):
                # append csv file name to values list
                values = re.sub( '\)$', csvFile, str(tuple(record)) )
                insert_statement += values 
                
                # Add terminator to values list
                if idx == len(rdr.records)-1:
                    insert_statement += ';'
                else:
                    insert_statement += ',\n'
            
            # Perform the INSERT 
            cursor.execute( insert_statement )
            
            # Update the status of file
            filesImported_done = '''UPDATE `filesImported` SET processed=1 WHERE id=%s; '''%(filesImported_id)

            cursor.execute( filesImported_done )
            
            self.db.commit()
            cursor.close()
            self.log( '''%s: %s records processed'''%(rdr.file, rdr.num_records))
            return True

        except MySQLdb.Error, e:
            self.log( "MySQL Error %d: %s" % (e.args[0], e.args[1]) )
            self.db.rollback()
            return False



    def loadRDR(self, rdr):
        '''
        Add records from RDR file in to database using LOAD statement
        Better performance than insertRDR(), but adds blank lines at end of CSV
          with a row for each line containing all NULL values
        '''
        
        if self.existsRDR(rdr):
            self.log('''%s: Exists. Skipping'''%(rdr.file))
            return False
        
        try:
            # Start database cursor
            cursor = self.db.cursor()
            
            table = self.translations[rdr.tag]
            
            # FILES IMPORTED
            filesImported_insert = '''INSERT INTO `filesImported`( rdr, filename, rows, md5sum, sha1_hash, processed) VALUES('%s', '%s', '%s', '%s', '%s', '%s')'''%(rdr.tag, rdr.path_target, rdr.num_records, rdr.md5sum, rdr.sha1_hash, 0)
            cursor.execute( filesImported_insert )
            filesImported_id = cursor.lastrowid
            
            # Remove apostrophe (') from table column names
            columns = re.sub( "'", "", str(self.tableDefinitions[rdr.tag]) )
            
            insert_statement = '''LOAD DATA INFILE '%s' INTO TABLE `%s` FIELDS TERMINATED BY ',' ENCLOSED BY '' LINES TERMINATED BY '\n' %s  SET csvFile = '%s';'''%(rdr.file, table, columns, filesImported_id)
            
            # Perform the INSERT 
            cursor.execute( insert_statement )
            
            # Update the status of file
            filesImported_done = '''UPDATE `filesImported` SET processed=1 WHERE id=%s; '''%(filesImported_id)
            cursor.execute( filesImported_done )
            
            self.db.commit()
            cursor.close()
            self.log( '''%s: %s records processed'''%(rdr.file, rdr.num_records))
            return True

        except MySQLdb.Error, e:
            self.log( "MySQL Error %d: %s" % (e.args[0], e.args[1]) )
            self.db.rollback()
            return False
           
           
                       
    def truncateTables(self):
        '''
        TEMP: Delete all records from tables
        '''
        
        try:
            # Start database cursor
            cursor = self.db.cursor()
            
            # FILES IMPORTED
            sql_truncate = 'TRUNCATE TABLE filesImported;'
            cursor.execute( sql_truncate )
            self.log( '''TRUNCATE: filesImported''' )
            
            for tag, table in self.translations.items():
                sql_truncate = '''TRUNCATE TABLE %s'''%(table)
                cursor.execute( sql_truncate )
                self.log( '''TRUNCATE: %s - %s'''%(tag, table))

            self.db.commit()
            cursor.close()

        except MySQLdb.Error, e:
        #except:
            self.log( "Error %d: %s" % (e.args[0], e.args[1]) )
            self.log( '''Error - Could not truncate tables.''')
            self.log( str( sys.exc_info() ) )
            return False
        
        
        
    def log(self, message):
        '''
        Output error to either stdout or syslog
        '''
        # Method options stdout or syslog
        methods = [ 'stdout', 'syslog' ]
        method = methods[1]
        try:
            if method == 'syslog':
                syslog.syslog( str(message) )
            else:
                print message
            return True
        except:
            print '''Error - cannot log messages.'''
            print str( sys.exc_info() )
            return False

