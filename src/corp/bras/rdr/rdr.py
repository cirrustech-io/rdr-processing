'''
Created on Jun 11, 2012

@author: Ali Khalil
'''

import sys
import syslog
import datetime
import os
import csv
import shutil
import hashlib


class RDR(object):

    def __init__(self, file):
        '''
        Initialize the NpcdbDump Object
        '''
        try:
            # Set RDR file properties
            self.file = file
            
            self.path_source = ''
            self.path_target = ''
            
            self.records = []
            self.num_records = 0
            self.md5sum = ''
            self.info = {}
            self.timespan = {}
            self.tag = 0
            
        except:
            self.log('''INIT: Error - Couldn't initialize file instance.''')
    
    
    def process(self):
        '''
        Process the RDR/CSV file and extract all required information
        '''
        try:
            # Check file data
            self.getFileRecords()

            # Process only if file has records 
            if self.num_records:
                self.getHashes()
                self.getFileStat()
                self.getTimespan()
                self.genArchiveTarget()
                return True
            else:
                return False
        except:
            self.log( '''Problem getting file information''' )
            self.log( str( sys.exc_info() ) )
            return False
    
    
    def __del__(self):
        '''
        Cleanup
        '''
        pass
        

    def getFileStat(self):
        '''
        Get the stat information for the file (full path)
        
        os.stat returns the following:
            0 - st_mode - protection bits,
            1 - st_ino - inode number,
            2 - st_dev - device,
            3 - st_nlink - number of hard links,
            4 - st_uid - user id of owner,
            5 - st_gid - group id of owner,
            6 - st_size - size of file, in bytes,
            7 - st_atime - time of most recent access,
            8 - st_mtime - time of most recent content modification,
            9 - st_ctime - platform dependent; time of most recent metadata change on Unix, or the time of creation on Windows)
        '''
        try:
            stat = os.stat( self.file )
            self.info = {
                            'size': stat[6],    # File size
                            'modified': stat[8] # modifed timestamp
                           }
            return True
        except:
            self.log( '''Problem getting file information''' )
            self.log( str( sys.exc_info() ) )
            return False
    
    
    def getTimespan(self):
        '''
        Get the first and last timestamp from RDR file
        
        records[record][column]
        i.e.
            records[0][0] - first row, first column
            records[last][0] - last row, first column
        
        '''
        try:
            #print self.records
            last = len(self.records)-1
            # Timestamps dividied by 1000 due to higher precision in RDR output
            self.timespan = {
                          'begin': int(self.records[0][0])/1000,
                          'end': int(self.records[last][0])/1000
                          }
            return True
        except:
            self.log( '''Problem getting timespan''' )
            self.log( str( sys.exc_info() ) )
            return False
    
    
    
    def getFileRecords(self):
        '''
        Read all rows from the RDR file
        '''
        try:
            csv_fh = open( self.file, 'rb' )
            csv_data = csv.reader( csv_fh )
            
            records = []
            
            for idx, row in enumerate(csv_data):
                # Only process if row contains data i.e. do nor process blank lines in csv file
                if row:
                    records.append( row )
                    if idx == 0:
                        self.tag = int(row[1])
                
            self.records = records
            self.num_records = len( records )
            if self.num_records < 1:
                self.log( '''Blank file: %s'''%(self.file))
            return True
        except:
            self.log( '''Error - Could not get file records.''' )
            self.log( str( sys.exc_info() ) )
            return False


    def getHashes(self):
        '''
        Extract the MD5sum value for the file
        '''
        try:
            #import hashlib
            md5 = hashlib.md5()
            sha1 = hashlib.sha1()
            with open(self.file,'rb') as f: 
                for chunk in iter(lambda: f.read(8192), b''): 
                    md5.update(chunk)
                    sha1.update(chunk)
            self.md5sum = md5.hexdigest()
            self.sha1_hash = sha1.hexdigest()
            return True
        except:
            self.log( '''Error - Cannot get MD5sum ''' )
            self.log( str( sys.exc_info() ) )
            return False
    

    def genArchiveTarget(self):
        '''
        Get full file path as input and generate the target archive path based on the timespan specified
        '''
        try:
            self.path_source = os.path.dirname( self.file )
            # target filename
            dt = datetime.datetime.fromtimestamp(self.timespan['begin'])
            # Generate target location for file - dirty name cleaned up using os.path.normpath
            target = '''%s/%s/%s.csv'''%(self.path_target,self.tag,dt.strftime('%Y/%m/%d/%Y%m%d_%H%M%S'))
            # Cleaning up target path 
            self.path_target = os.path.normpath( target)
            return True
        except:
            self.log( '''Error - Could not generate target information for rdr file''' )
            self.log( str( sys.exc_info() ) )
            return False
    
    
    def archive(self):    
        '''
        Archive RDR file by moving it to target destination and renaming file
        '''
        try:
            # Check to see if directory exists
            target_dir = os.path.dirname(self.path_target)
            if not os.path.isdir(target_dir):
                os.makedirs(target_dir)
                self.log( '''Target directory created: %s'''%(target_dir))
                
            shutil.move(self.file, self.path_target)
            #shutil.copy(self.file, self.path_target)
            self.log( '''%s: moved to %s'''%(self.file, self.path_target))
            
            return True
        except:
            self.log( '''Error - Could not archive rdr file''' )
            self.log( str( sys.exc_info() ) )
            return False
            
    def delete(self):    
        '''
        Archive RDR file by moving it to target destination and renaming file
        '''
        try:
            os.remove(self.file)
            self.log( '''Deleted file: %s'''%(self.file))
            return True
        except:
            self.log( '''Error - Could not delete blank rdr file''' )
            self.log( str( sys.exc_info() ) )
            return False


    def log(self, message):
        '''
        Output error to either stdout or syslog
        '''
        # Method options stdout or syslog
        methods = [ 'stdout', 'syslog' ]
        method = methods[1]
        try:
            if method == 'syslog':
                syslog.syslog( str(message) )
            else:
                print message
            return True
        except:
            print '''Error - cannot log messages.'''
            print str( sys.exc_info() )
            return False
    